'use strict';

// console.log('\'Allo \'Allo! Popup');

var HASMOCK = 'HAS_MOCK_DATA';
var mockCookie;
var reload_scheduled;
var USER_CONFIG = { programType: '' };

function CookieCache() {
  this.cookies_ = {};

  this.reset = function() {
    this.cookies_ = {};
  };

  this.add = function(cookie) {
    var domain = cookie.domain;
    if (!this.cookies_[domain]) {
      this.cookies_[domain] = [];
    }
    this.cookies_[domain].push(cookie);
  };

  this.remove = function(cookie) {
    var domain = cookie.domain;
    if (this.cookies_[domain]) {
      var i = 0;
      while (i < this.cookies_[domain].length) {
        if (cookieMatch(this.cookies_[domain][i], cookie)) {
          this.cookies_[domain].splice(i, 1);
        } else {
          i++;
        }
      }
      if (this.cookies_[domain].length == 0) {
        delete this.cookies_[domain];
      }
    }
  };

  // Returns a sorted list of cookie domains that match |filter|. If |filter| is
  //  null, returns all domains.
  this.getDomains = function(filter) {
    var result = [];
    sortedKeys(this.cookies_).forEach(function(domain) {
      if (!filter || domain.indexOf(filter) != -1) {
        result.push(domain);
      }
    });
    return result;
  };

  this.getCookies = function(domain) {
    return this.cookies_[domain];
  };

  this.getAllCookies = function() {
    return this.cookies_;
  };
}

var cache = new CookieCache();

// function listener(info) {
//   cache.remove(info.cookie);
//   if (!info.removed) {
//     cache.add(info.cookie);
//   }
// }

// function startListening() {
//   chrome
//     .cookies
//     .onChanged
//     .addListener(listener);
// }

// function stopListening() {
//   chrome
//     .cookies
//     .onChanged
//     .removeListener(listener);
// }

function Timer() {
  this.start_ = new Date();

  this.elapsed = function() {
    return new Date() - this.start_;
  };

  this.reset = function() {
    this.start_ = new Date();
  };
}

function onload(callback) {
  var timer = new Timer();
  chrome.cookies.getAll({}, function(cookies) {
    // startListening();
    // var start = new Date();
    for (var i in cookies) {
      cache.add(cookies[i]);
      var cookie = cookies[i];
      if (cookie.name === HASMOCK) {
        mockCookie = cookie;
      }
    }

    timer.reset();

    callback();
  });
}

$(function() {
  var $mycheckbox = $('#my-checkbox');
  var tempMockCookie = {
    domain: 'minner.jr.jd.com',
    url: 'http://minner.jr.jd.com/',
    httpOnly: false,
    name: HASMOCK,
    path: '/',
    sameSite: 'no_restriction',
    secure: false,
    storeId: '0',
    value: 'true'
  };
  onload(function() {
    // console.log(JSON.stringify(cache.getCookies('minner.jr.jd.com')[0]), mockCookie)
    if (mockCookie && mockCookie.value) {
      $mycheckbox.attr('checked', 'checked');
    } else {
      $mycheckbox.removeAttr('checked');
    }

    var $myCheckBoxSwitch = $mycheckbox.bootstrapSwitch();
    $myCheckBoxSwitch.on('switchChange.bootstrapSwitch', function(event, state) {
      // console.log(this); // DOM element
      // console.log(event); // jQuery event
      // console.log(state); // true | false
      if (state) {
        chrome.cookies.set(tempMockCookie, function() {
          console.log('add success');
          chrome.tabs.executeScript({ code: 'window.location.reload(true)' });
        });
      } else {
        chrome.cookies.remove(
          {
            url: tempMockCookie.url,
            name: tempMockCookie.name
          },
          function() {
            console.log('remove success');
            chrome.tabs.executeScript({ code: 'window.location.reload(true)' });
          }
        );
      }
    });

    var $radioBtn = $('.form-check .form-check-input');

    chrome.storage.sync.get('user_config', function(values) {
      if (values && values.user_config) {
        $("input[name='exampleRadios'][value=" + values.user_config.programType + ']').attr('checked', true);
      }
    });

    $radioBtn.change(function() {
      var $this = $(this);
      var val = $this.val();

      USER_CONFIG.programType = val;

      chrome.storage.sync.set({ user_config: USER_CONFIG }, function() {
        chrome.tabs.executeScript({ code: 'window.location.reload(true)' });
      });
    });
  });
});
