import React, {StyleSheet, Dimensions, PixelRatio} from 'react-native';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "paddingTop": 10,
        "paddingRight": 10,
        "paddingBottom": 10,
        "paddingLeft": 10,
        "width": 260
    },
    "flex": {
        "display": "flex",
        "WebkitBoxOrient": "vertical",
        "WebkitBoxDirection": "normal",
        "MozBoxOrient": "vertical",
        "MozBoxDirection": "normal"
    },
    "flex1": {
        "WebkitBoxFlex": 1,
        "WebkitFlex": 1,
        "MsFlex": 1,
        "flex": 1
    },
    "flexRow": {
        "WebkitFlexDirection": "row",
        "MsFlexDirection": "row",
        "flexDirection": "row"
    },
    "flexColumn": {
        "WebkitFlexDirection": "column",
        "MsFlexDirection": "column",
        "flexDirection": "column"
    },
    "flexVHCenter": {
        "WebkitBoxAlign": "center",
        "WebkitAlignItems": "center",
        "alignItems": "center",
        "WebkitBoxPack": "center",
        "WebkitJustifyContent": "center",
        "justifyContent": "center"
    },
    "flexVCenter": {
        "WebkitBoxPack": "center",
        "WebkitJustifyContent": "center",
        "justifyContent": "center"
    },
    "flexHCenter": {
        "WebkitBoxAlign": "center",
        "WebkitAlignItems": "center",
        "alignItems": "center"
    },
    "bfc": {
        "overflow": "hidden"
    },
    "bfc:after": {
        "display": "block",
        "visibility": "hidden",
        "overflow": "hidden",
        "content": "''",
        "clear": "both"
    },
    "bfc:before": {
        "display": "block",
        "visibility": "hidden",
        "overflow": "hidden",
        "content": "''"
    },
    "list": {
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0
    },
    "item": {
        "height": 32
    },
    "item-lable": {
        "fontSize": 14,
        "lineHeight": 32,
        "paddingRight": 10
    }
});