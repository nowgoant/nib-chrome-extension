console.log('content_script_start');
var user_config = { programType: '' };
chrome.storage.sync.get('user_config', function(values) {
  if (values && values.user_config) {
    user_config = values.user_config;
  }
});

document.addEventListener('DOMContentLoaded', function() {});

function toUnit(node) {
  var val = node.nodeValue.replace('px', '');
  switch (user_config.programType) {
    case 'rpx':
      return val + user_config.programType;
    case 'rem':
      return '$rem' + val;
    default:
      return val + 'px';
  }
}

$(document).on('click', function(e) {
  var stt = setTimeout(function() {
    clearTimeout(stt);
    var $code = $('.code_detail .code_box code');
    // console.log('$code', $code);
    if ($code[0] && $code[0].childNodes) {
      $code[0].childNodes.forEach(function(node) {
        if (node.nodeName.indexOf('#text') > -1 && node.nodeValue.indexOf('px') > -1) {
          console.log('nodeValue', node.nodeValue);
          var split = node.nodeValue.split(' ');
          if (split && split.length > 1) {
          } else {
            node.nodeValue = toUnit(node);
          }
        }
      });
    }
  }, 300);
});
