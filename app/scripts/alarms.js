'use strict';

var alarmName = 'remindme';
function checkAlarm(callback) {
  chrome.alarms.getAll(function (alarms) {
    var hasAlarm = alarms.some(function (a) {
      return a.name == alarmName;
    });
    var newLabel;
    if (hasAlarm) {
      newLabel = 'Cancel alarm';
    } else {
      newLabel = 'Activate alarm';
    }
    document.getElementById('toggleAlarm').innerText = newLabel;
    if (callback) callback(hasAlarm);
  });
}
function createAlarm() {
  chrome.alarms.create(alarmName, {
    delayInMinutes: 0.1,
    periodInMinutes: 0.1
  });
}
function cancelAlarm() {
  chrome.alarms.clear(alarmName);
}

function doToggleAlarm() {
  checkAlarm(function (hasAlarm) {
    if (hasAlarm) {
      cancelAlarm();
    } else {
      createAlarm();
    }
    checkAlarm();
  });
}

$('#toggleAlarm').on('click', doToggleAlarm);

// checkAlarm();

chrome.alarms.onAlarm.addListener(function (alarm) {
  console.log('Got an alarm!', alarm);
  chrome.notifications.create('reminder', {
    type: 'basic',
    iconUrl: 'images/icon-38.png',
    title: 'Dont forget!',
    message: 'You have things to do. Wake up, dude!'
  }, function (notificationId) {
    console.log('arguments', notificationId);
  });
});